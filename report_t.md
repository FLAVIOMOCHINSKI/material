# Entrega dos exercícios

- **Grupo**: ds122-2022-2-t
- **Última atualização**: ter 08 nov 2022 16:36:16 -03

|Nome|	ds122-prepare-assignment<br>2022-11-01|	ds122-http-assignment<br>2022-11-01|	ds122-html-tables-assignment<br>2022-11-08|	ds122-html-store-assignment<br>2022-11-08|
|----|	:---:|	:---:|	:---:|	:---:|
|ALIRES_DOMENIQUE_MOREIRA_ROSA|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|ANDRE_ALEX_JANKOSKI|	 ok |	 ok |	 ok |	 ok |
|CAIO_YUZO_HIRAGA|	 ok |	 ok |	 ok |	 ok |
|CAMILA_ISABELLA_VERSSÃO|	 Fork, mas nenhum commit até data de entrega|	 ok |	 ok |	 ok |
|CARLOS_EDUARDO_CAMARGO_VIANA|	 Fork, mas nenhum commit até data de entrega|	 ok |	 ok |	 ok |
|CHRISTIAN_DOS_SANTOS_EURINIDIO|	 ok |	 ok |	 ok |	 ok |
|CHRISTIAN_KENNEDY_DANIEL_MARTINS|	 ok |	 ok |	 ok |	 ok |
|CHRISTOPHER_PICOLOTTO_RODRIGUES|	 ok |	 ok |	 ok |	 ok |
|CRISTIANO_CÉZAR_MONTEIRO_PAULINO|	 ok |	 ok |	 ok |	 ok |
|DIEGO_ALVES_DA_COSTA|	 ok |	 ok |	 ok |	 ok |
|EDUARDO_FELIX_DA_SILVA_NETO|	 ok |	 ok |	 ok |	 ok |
|EDUARDO_HENRIQUE_ALBERTI_COSTA|	 ok |	 ok |	 ok |	 ok |
|FERNANDO_GUILHERME_GORSKI|	 ok |	 ok |	 ok |	 ok |
|GABRIEL_KAUÊ_CAITANO_ALVES|	 ok |	 ok |	 ok |	 ok |
|GABRIEL_MENDES_TULIO|	 ok |	 ok |	 ok |	 ok |
|GIULIA_MARIANE_BRANCALHÃO|	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|GUILHERME_FERREIRA_RODRIGUES|	 Fork, mas nenhum commit até data de entrega|	 ok |	 Fork não encontrado |	 ok |
|GUILHERME_RONCON_MALTA|	 ok |	 ok |	 ok |	 ok |
|HYGOR_ADRIANO_TRISTÃO|	 ok |	 ok |	 ok |	 ok |
|INGRIDHY_CRYSTYNA_TONIOLO_DE_SOUZA|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|JOAO_JOSE_MOREIRA_RAMOS_NETO|	 ok |	 ok |	 ok |	 ok |
|JOAO_VITOR_ARAUJO_DOS_SANTOS|	 ok |	 ok |	 ok |	 ok |
|JULIA_DEL_CARMEN_PASCUAL_ANDRADE|	 ok |	 ok |	 ok |	 ok |
|LEONARDO_DEMATÉ_BAUCKE|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|LÍVIA_DE_ARAÚJO_NUNES|	 ok |	 ok |	 ok |	 ok |
|LUCAS_MASSAYUKI_GOHARA|	 ok |	 ok |	 ok |	 ok |
|LUIGI_LEDERMANN_GIRARDI|	 ok |	 ok |	 ok |	 ok |
|LUIZ_HENRIQUE_SCHECHELI_BUSSOLO|	 ok |	 ok |	 ok |	 ok |
|LUIZ_ROGÉRIO_ALVES_DOS_SANTOS|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|MARCELLY_MACIEL_DE_OLIVEIRA|	 ok |	 ok |	 ok |	 ok |
|MARCOS_FELIPE_LOPES_RODRIGUES|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|MARIA_EDUARDA_MUNCINELLI|	 ok |	 ok |	 ok |	 ok |
|MARTHAN_RIBEIRO_MEIRA|	 ok |	 Fork não encontrado |	 ok |	 Fork, mas nenhum commit até data de entrega|
|MATHEUS_DE_OLIVEIRA_MUNARETTO|	 Fork, mas nenhum commit até data de entrega|	 ok |	 ok |	 ok |
|MAURITS_ALBERT_STRIJKER|	 ok |	 ok |	 ok |	 ok |
|MELISSA_SILVA_DE_OLIVEIRA|	 ok |	 ok |	 ok |	 ok |
|MICKAEL_JOSÉ_SUOTA|	 Fork não encontrado |	 Fork não encontrado |	 ok |	 Fork, mas nenhum commit até data de entrega|
|NATASHA_ALCAIDE_SANTOS|	 ok |	 ok |	 ok |	 ok |
|RAFAEL_LEAL_DANTAS|	 Fork não encontrado |	 ok |	 ok |	 ok |
|RAFAEL_PINTO_CARDOSO|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|RAFAELA_TREVISAN|	 ok |	 ok |	 ok |	 ok |
|RHUAN_VINICIUS_RODRIGUES_MARTINS|	 ok |	 ok |	 ok |	 ok |
|RODRIGO_BARBOSA_DA_SILVA|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|RUI_ANTONIO_DOS_SANTOS_JUNIOR|	 ok |	 ok |	 ok |	 ok |
|SAMUEL_DOMINGUEZ_SANTOS|	 ok |	 ok |	 ok |	 ok |
|THALYSON_BRUCK_ANDREATTA|	 ok |	 ok |	 ok |	 ok |
|THÉO_DREER_TAVARES|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|VITÓRIA_DE_FREITAS_MACHADO_DA_SILVA|	 ok |	 ok |	 ok |	 ok |
