# Entrega dos exercícios

- **Grupo**: ds122-2022-2-n
- **Última atualização**: qua 09 nov 2022 18:53:40 -03

|Nome|	ds122-prepare-assignment<br>2022-10-27|	ds122-http-assignment<br>2022-10-09|
|----|	:---:|	:---:|
|ADRIANO_MONTAGUTI|	 ok |	 ok |
|ALINE_MACHADO_LIMA|	 ok |	 ok |
|ANDERSON_SOUZA_SANTOS|	 ok |	 ok |
|ANDRÉ_ANASTÁCIO_DE_OLIVEIRA|	 ok |	 ok |
|ANDRÉ_LUIZ_LEME|	 Fork não encontrado |	 Fork não encontrado |
|BRUNO_CARLOS_DE_SOUZA_BLETES|	 ok |	 ok |
|CAIO_EDUARDO_MOREIRA_DE_LIMA|	 ok |	 ok |
|CAROLINA_DOS_SANTOS_DE_LIMA|	 ok |	 ok |
|CAROLINE_PICANCO_PROCKMANN|	 ok |	 ok |
|CRISTIANO_MARTINS_DE_SOUZA|	 ok |	 ok |
|DIEGO_ALVES_LISBOA|	 Fork não encontrado |	 Fork não encontrado |
|DOUGLAS_TIAGO_VALÉRIO|	 Fork não encontrado |	 Fork não encontrado |
|EDUARDO_COSTA_MATOS|	 ok |	 ok |
|EDUARDO_VINICIUS_FROHLICH|	 ok |	 ok |
|ESDRAS_EMANUEL_ALVES_BEZERRA|	 Fork não encontrado |	 Fork não encontrado |
|EVERTON_DA_SILVA_VIEIRA|	 ok |	 ok |
|FLÁVIO_LUÍS_MOCHINSKI|	 Fork não encontrado |	 Fork não encontrado |
|GABRIEL_FELIPE_BATISTA_DOS_SANTOS|	 Fork não encontrado |	 Fork não encontrado |
|GABRIEL_HENRIQUE_DE_ASSIS_DA_SILVA|	 Fork não encontrado |	 Fork não encontrado |
|GABRIEL_HISSATOMI_DAVANZO|	 ok |	 ok |
|GABRIELA_SCHNEIDER_LOPES|	 ok |	 ok |
|GUSTAVO_ANTONIO_PADILHA|	 ok |	 ok |
|GUSTAVO_IASSYL_NIELSEN_ALVES|	 ok |	 ok |
|GUSTAVO_JAHNZ|	 ok |	 ok |
|HENRIQUE_ULBRICH_DE_SOUZA|	 Fork não encontrado |	 ok |
|JAVAN_MACHADO_BONFIM|	 ok |	 ok |
|JOÃO_GUILHERME_ALMEIDA_COUTO|	 ok |	 ok |
|JORGE_VICTOR_BENCKE_GASTALDI|	 Fork não encontrado |	 Fork não encontrado |
|LAISA_CRISTINA_KROLIKOVSKI_DA_SILVA|	 ok |	 ok |
|LEONARDO_DE_LIMA_PEREIRA|	 ok |	 ok |
|LUCAS_DOS_SANTOS_SANTANA_PINTO|	 ok |	 Fork não encontrado |
|LUCAS_FRACARO_NUNES|	 ok |	 ok |
|LUCAS_HENRIQUE_RAMOS_CAZIONATO|	 ok |	 ok |
|LUCAS_VINICIUS_DE_CARVALHO|	 Fork não encontrado |	 Fork não encontrado |
|LUIS_FERNANDO_TUROZI_MAUSSON|	 ok |	 Fork não encontrado |
|MARCEL_ALESSANDRO_ZIMMER|	 ok |	 ok |
|MARCEL_RODRIGUES_CARDOZO|	 Fork não encontrado |	 Fork não encontrado |
|MARIANE_ROESLER|	 ok |	 ok |
|MARICOT_NICOLAS|	 ok |	 ok |
|MATHEUS_DIONYSIO_CLASSE|	 ok |	 ok |
|MATHEUS_HENRIQUE_MIRANDA|	 ok |	 ok |
|MATHEUS_RIGLER|	 Fork não encontrado |	 Fork não encontrado |
|PABLO_ALVES_FERNANDES|	 Fork não encontrado |	 Fork não encontrado |
|RAPHAEL_PERES_DE_OLIVEIRA|	 ok |	 ok |
|RUAN_SERGIO_CUNHA_BRITO|	 Fork não encontrado |	 Fork não encontrado |
|RUPÉLIO_COLFERAI_JUNIOR|	 ok |	 ok |
|VICTORIA_KUNZ_VIEIRA|	 ok |	 ok |
|VINÍCIUS_ALVES_DOS_SANTOS|	 ok |	 ok |
|VINICIUS_GUSTAVO_RAZOTO|	 ok |	 ok |
|VITÓRIA_LAÍS_SOUZA_DOS_SANTOS|	 ok |	 Fork não encontrado |
|WILLIAM_MATHEUS_DEL_PONTE_FERREIRA|	 ok |	 ok |
|YASMIN_TAINÁ_DA_SILVA|	 Fork não encontrado |	 Fork não encontrado |
